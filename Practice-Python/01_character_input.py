# Create a program that asks the user to enter their name and their age.
# Print out a message addressed to them that tells them the year that they will turn 100 years old.
#
# Extras:
#
# Add on to the previous program by asking the user for another number
# and printing out that many copies of the previous message. (Hint: order of operations exists in Python)
# Print out that many copies of the previous message on separate lines.
# (Hint: the string "\n is the same as pressing the ENTER button)
import datetime

dt = datetime.datetime.today()


def get_int(text):
    try:
        return int(input(text))
    except:
        print("Please enter a number")


def get_str(text):
    while True:
        try:
            return str(input(text))
        except:
            print("Please use letters only")


print("Hi there!")
while True:
    name = input("What's your name?\n")
    if not name.isalpha():
        print("Please use letters only")
    else:
        print(f"Nice to meet you, {name}.")
        while True:
            age = get_int("How old are you?\n")
            if type(age) != int:
                continue
            else:
                while True:
                    repeat_no = get_int("How many times do you want to see the message displayed?\n")
                    if type(repeat_no) != int:
                        continue
                    elif repeat_no < 1:
                        print("Please pick a number larger than 1.")
                    elif repeat_no >= 1:
                        year_at_100 = (dt.year - age) + 100
                        if age < 100:
                            print(f"Your 100th birthday will be in year {year_at_100}.\n" * repeat_no)
                        else:
                            print(f"Your 100th birthday was in year {year_at_100}.\n"
                                  f"Congratulations! May you live another 100 years\n"
                                  f"and more!\n" * repeat_no)
                        exit()
