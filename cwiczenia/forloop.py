def forloop(number):
    global counter
    global summ
    summ = 0
    counter = number
    for elem in range(number):
        summ = elem + 1 + summ
        print(counter, elem +1)
    return summ, counter


if __name__ == '__main__':
    num = int(input("Podaj liczbę: "))
    forloop(num)
    print(f"Suma pierwszych liczb dla wartości {num} wynosi {summ}. Operację wykonano w {num} krokach.")

