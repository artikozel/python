import random


def get_int(text):
    try:
        return int(input(text))
    except:
        print("Wybierz liczbę!")

def get_str(text):
    try:
        return str(input(text))
    except:
        print("Wybierz opcję!")

# get_int("Podaj liczbę")


if __name__ == '__main__':

    points = 0
    while True:
        level = get_int("Wybierz poziom trudności (1 - 100 000): ")
        # print(level)
        if type(level) == int:
            if level > 100000:
                print("Wybrałeś zbyt trudny poziom")
            elif level < 1:
                print("Wybrałeś zbyt niski poziom")
            if 1 <= level <= 100000:
                while True:
                    u_choice = get_str("Gotowy na grę? [yes/return/exit]: ")
                    x = random.randint(1, level)
                    if u_choice == 'exit':
                        print("Pa pa!")
                        exit()
                    elif u_choice == 'yes':
                        # print(x)
                        while True:
                            print(f"High score: {points}")
                            u_input = get_int("Zgadnij, o jakiej liczbie myślę!")
                            # print(x)
                            if type(u_input) != int:
                                continue
                            elif u_input < x:
                                print("Zbyt nisko!")
                            elif u_input > x:
                                print("UUu! Zbyt wysoko!")
                            elif u_input == x:
                                print("Brawo! Dobra robota!")
                                points += 1
                                break
                    elif u_choice == "return":
                        break
                    elif u_choice != "yes" or "return" or "exit":
                        print("Zdecyduj się!")
