
set_one = {1, 2, 3, 4, 5}
print("set one", set_one)
set_two = {4, 5, 6, 7, 8}
print("set two", set_two)
print("set one & set two: \"set_one | set_two\"", set_one | set_two)
print("LEN set one & set two", len(set_one | set_two))
print("set one and two intersection: \"set_one & set_two\"", set_one & set_two)
print("LEN intersection set one & set two", len(set_one & set_two))
print("set one minus set two: \"set_one - set_two\"", set_one - set_two)
print("set two minus set one: \"set_two - set_one\"", set_two - set_one)
print("set one and two without duplicates: \"set_one ^ set_two\"", set_one ^ set_two)

list_with_duplicates = [1, 2, 3, 1, 2, 3]
print("list w duplicates: \"list_with_duplicates\"", list_with_duplicates)
list_without_duplicates = list(set(list_with_duplicates))
print("list without duplicates: \"list(set(list_with_duplicates))\"", list_without_duplicates)

set_one.add(10)
print(set_one)
set_one.remove(10)
print(set_one)
set_one.update({666, 667, 668})
print(set_one)
x = set()
x.update({666, 667, 668})
print(x)

print(5 in set_one)
print(7 in set_one)
print(set_one.issubset({1, 2, 3}))
print(set_one.issuperset({1, 2, 3}))
