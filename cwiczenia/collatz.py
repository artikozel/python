def collatz(any_number):
    print(any_number)
    if any_number == 1:
        return
    elif any_number % 2:
        return collatz(any_number *3 +1)
    else:
        return collatz(any_number // 2)

def get_int(whole_number):
    while True:
        try:
            return int(input(whole_number))
        except:
            print("Podana liczba nie jest liczbą całkowitą: ")


if __name__ == '__main__':
    number = get_int("Podaj liczbę całkowita: ")
    print(f'The Collatz sequence for {number} is: ')
    collatz(number)