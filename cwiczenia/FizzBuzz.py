# Print out numbers from 1 to 100 (including 100).
# But, instead of printing multiples of 3, print "Fizz"
# Instead of printing multiples of 5, print "Buzz"
# Instead of printing multiples of both 3 and 5, print "FizzBuzz".

counter = 1
while counter <= 100:
    if counter % 3 == 0 and counter % 5 == 0:
        print(counter, "FizzBuzz")
        counter += 1
    elif counter % 5 == 0:
        print(counter, "Buzz")
        counter += 1
    elif counter % 3 == 0:
        print(counter, "Fizz")
        counter += 1
    else:
        print(counter)
        counter += 1
