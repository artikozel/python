# Utwórz nowy plik np. jednostki.py, który po podaniu przez użytkownika długości w cm będzie wyświetlał wynik w metrach i calach zaokrąglając do 4 miejsc po przecinku.
# Podobny skrypt możesz wykonać dla zamiany kg na funty.
# Wynik wyświetl używając dowolnego sposobu formatowania tekstu.

def conversion(value):
    value_m = value / 100
    value_in = value / 0.393701
    return value_m, value_in
def conversion_pounds(value_kg):
    value_pnd = value_kg * 2.20462
    return value_pnd

if __name__ == '__main__':
    value = int(input("Podaj długość [cm]: "))
    value_kg = float(input("Podaj wagę [kg]: "))
    m, inch = conversion(value)
    pnd = conversion_pounds(value_kg)
    print(f"Podana długość to {m} metrów.")
    print(f"Podana długość to {inch:.4f} cali.")
    print(f"Podana waga to {pnd:.4f} funtów.")
