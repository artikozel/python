# 3. Napisz funkcję reverse_list, która przyjmuje listę processed_list.
# Jej zadaniem jest obrócenie kolejności elementów w tej liście i zwrócenie odwróconej listy (przykładowo, dla listy [1, 2, 3, 4],
# funkcja powinna zwrócić listę [4, 3, 2, 1]). Uwaga: nie używaj wbudowanej funkcji reversed ani metody .reversed.

def reverse_list(processed_list):
    list_reversed = processed_list[::-1]
    return (list_reversed)


def reverse_list2(processed_list):
    list_reversed = processed_list[::-1] #for loop
    print(list_reversed)

if __name__ == '__main__':
    my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print(reverse_list(my_list))
