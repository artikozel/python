# 2. Napisz funkcję sum_up, która przyjmuje liczbę number.
# Jej zadaniem jest obliczanie sumy wartości od 0 do zadanej wartości number (przykładowo, sum_up(3) powinno zwrócić liczbę 1+2+3=6,
# natomiast sum_up(5) powinno zwrócić 1+2+3+4+5=15).

def sum_up(some_number):
   sum = 0
   for elem in range(some_number):
       sum = sum + elem+1
       print(elem+1)
   return sum

if __name__ == '__main__':
    num = input("Podaj liczbę: ")
    print("Suma pierwszych liczb to:", sum_up(int(num)))

# def sum_up(my_list):
#    sum = 0
#    for elem in my_list:
#        sum = sum + elem+1
#        print(elem+1)
#    return sum
#
# if __name__ == '__main__':
#     my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#     print(sum_up(my_list))