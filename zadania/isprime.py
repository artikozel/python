# # 1. Napisz funkcję prime_numbers, która przyjmować będzie liczbę quantity.
# # Jej zadaniem jest wyświetlenie quantity liczb pierwszych.
# # Liczba pierwsza to taka liczba, która jest podzielna tylko przez 1 oraz samą siebie.
# # Pomocniczo, opracuj funkcję is_prime, przyjmującą liczbę number,
# # która będzie sprawdzała, czy dana liczba jest liczbą pierwszą.
#
# # def prime_numbers(quantity):
def prime_numbers(quantity):
    number = 2
    prime_list = []
    while len(prime_list) < quantity:
        if is_prime(number) == True:
            prime_list.append(number)
        number += 1
        #print(f" Liczba {number} to liczba pierwsza. Dodano do listy")
        #print(f" Następna sprawdzana liczba to {number + 1}, długość prime_list to {len(prime_list)} a jej wartości to {prime_list}")
    else:
        number += 1
    return number
    print(prime_list)

# def prime_numbers(quantity):
#     number = 2
#     prime_list = []
#     while len(prime_list) < quantity:
#         print(f"Liczba sprawdzana to {number}, długość prime_list to {len(prime_list)}")
#         if is_prime(number) == True:
#             prime_list.append(number)
#             print(f" Liczba {number} to liczba pierwsza. Dodano do listy")
#             print(f" Następna sprawdzana liczba to {number + 1}, długość prime_list to {len(prime_list)} a jej wartości to {prime_list}")
#         else:
#             print(f"Liczba {number} nie jest liczbą pierwszą. Pomijamy.")
#             print(f" Następna sprawdzana liczba to {number+1}, długość prime_list to {len(prime_list)} a jej wartości to {prime_list}")
#         number += 1

# def is_prime(no):
#     if no % no == False and no % 2 == True:
#         if no == 2:
#             return True
#         elif no == 1:
#             return False
#         return True
#     else:
#         return False

def is_prime(no):
    if no > 1:
        for i in range(2, no):
            if (no % i) == 0:
                return False
        else:
            return True

if __name__ == '__main__':
    quantity = int(input("Podaj ilość liczb pierwszych do wyświetlenia: "))
    #print(is_prime(quantity))
    prime = prime_numbers(quantity)
    print(prime_numbers(quantity))
    print(prime)