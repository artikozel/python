#  1. Napisz funkcję divisible, która przyjmuje liczbę number oraz dzielnik divisor.
#  Jej zadaniem jest sprawdzenie, czy liczba jest podzielna przez dzielnik. Wykorzystaj w tym celu dzielenie modulo %.
#  Jeżeli liczba jest podzielna przez dzielnik, funkcja powinna zwrócić wartość True, jeżeli nie jest - wartość False.

def divisible(number, divisor):
    result = number % divisor
    if result == 0:
        return "Liczba podzielna"
    else:
        return "Liczba niepodzielna"

def get_int(tekst):
    while True:
        try:
            return int(input(tekst))
        except:
            print("to nie jest liczba...")

if __name__ == '__main__':
    number = get_int("Podaj liczbę: ")
    divisor = get_int("Podaj dzielnik: ")
    print('Wynik: ', number / divisor)
    print(divisible(number, divisor))

