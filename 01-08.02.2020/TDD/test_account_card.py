import unittest
from account import Account
from card import Card


class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.account = Account("1234567890", "Tomek", "Porwit", 15000 * 1.23)
        self.card = Card(self.account, "3241")

    def test_account_should_return_account_number(self):
        self.assertEqual(self.account(), "1234567890", "account number does not match")

    def test_accountowner_should_return_owner_name_and_surname(self):
        self.assertEqual(self.account.owner(), "Tomek Porwit", "name and surname do not match")

    def test_accountbalance_should_return_current_balance(self):
        self.assertEqual(self.account.balance(), 18450, "account balance does not match")

    def test_accounnumber_should_return_accoun_number(self):
        self.assertEqual(self.account.number(), "1234567890", "account number does not match")

    def test_accounttransfer_should_return_added_accountbalance(self):
        self.accout.transfer(1550)
        self.assertEqual(self.account.balance(), 20000, "balance does not match")

    def test_accounttransfer_should_return_subtracted_accountbalance(self):
        self.account.transfer(-3450)
        self.assertEqual(self.account.balance(), 15000, "balance does not match")

    def test_card_should_return_owner_name_and_surname(self):
        self.assertEqual(self.card(), "Tomek Porwit", "owner name does not match")

    def test_cardcheckpin_should_check_if_pin_is_correct(self):
        self.assertTrue(self.card.check_pin("3241"), "pin does not match")
    @unittest.skip("just because")
    def test_cardaccount_should_return_owner_account(self):
        self.assertEqual(self.card.get_account(), self.account, "accounts do not match")
        # self.assertIs(self.card.get_account(), self.account, "accounts do not match")

if __name__ == '__main__':
    unittest.main()
