import unittest
from card import *
from account import *


class Tdd_zad1(unittest.TestCase):
    #1. sprawdzic, czy saldo jest wieksze niz wyplata
    #2. sprawdzic, czy mozna wyplacic, jesli PIN jest niepoprawny


    def test_if_balance_is_higher_than_withdrawal_amount(self):

        account = Account("1234567890", "Tomek", "Porwit", 1000)
        card = Card(account, "1234")
        self.assertEqual(card.withdraw(1001, "1234"), 0)


    def test_withdraw_money(self):

        account = Account("1234567890", "Tomek", "Porwit", 1000)
        card = Card(account, "1234")

        self.assertEqual(card.withdraw(1000, "1234"), 1000)
        self.assertEqual(account.balance(), 0)


    def test_withdraw_if_pin_incorrect(self):

        account = Account("1234567890", "Tomek", "Porwit", 1000)
        card = Card(account, "1234")

        self.assertEqual(card.withdraw(1000, "0000"), 0)


    def test_PIN_verification_attempts_exceeded(self):
        #When
        account = Account("1234567890", "Tomek", "Porwit", 1000)
        card = Card(account, "1234")

        card.check_pin('0000')
        card.check_pin('0000')
        card.check_pin('0000')

        with self.assertRaises(PINVerificationAttemptsExceeded):
            card.check_pin('0000')

    def test_card_should_not_be_locked_if_at_least_one_correct_input(self):
        #When
        account = Account("1234567890", "Tomek", "Porwit", 1000)
        card = Card(account, "1234")

        self.assertFalse(card.check_pin('0000'))
        self.assertFalse(card.check_pin('0000'))
        self.assertTrue(card.check_pin('1234'))
        self.assertFalse(card.check_pin('0000'))
        self.assertFalse(card.check_pin('0000'))




    #1. przelać pieniadze na inne konto, tym samym obnizyc sobie saldo i powiekszyc drugiemu
    #2. chce sprawdzic, czy moge przelac pieniadze
    #3. chce uniemozliwic zlecenia przelewow z obcego konta na moje
    #4. chcę, żeby zapanował socjalizm


    def test_send_money_to_another_account(self):

        accountJacek = Account("1234567890", "Jacek", "Kowalski", 500)
        accountJan = Account("0987654321", "Jan", "Nowak", 1000)

        accountJacek.transfer_to_another_account(500, accountJan)

        self.assertEqual(accountJacek.accountBalance, 0)
        self.assertEqual(accountJan.accountBalance, 1500)

    def test_do_not_transfer_money_if_balance_not_enough(self):

        #Given
        accountJacek = Account("1234567890", "Jacek", "Kowalski", 500)
        accountJan = Account("0987654321", "Jan", "Nowak", 1000)

        #When
        accountJacek.transfer_to_another_account(1000, accountJan)

        #Then
        self.assertEqual(accountJacek.accountBalance, 500)
        self.assertEqual(accountJan.accountBalance, 1000)

    def test_do_not_allow_to_transfer_money_from_recipient_to_sender(self):

        # Given
        accountJacek = Account("1234567890", "Jacek", "Kowalski", 500)
        accountJan = Account("0987654321", "Jan", "Nowak", 1000)

        with self.assertRaises(UnsupportedTransactionError):
            accountJacek.transfer_to_another_account(-1000, accountJan)


if __name__ == '__main__':
    unittest.main()
