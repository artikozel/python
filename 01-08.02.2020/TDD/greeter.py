def greet(*names):
    names_size = len(names)
    names_uppercase = []
    names_normal = []
    for name in names:
        if name.isupper():
            names_uppercase.append(name.strip())
        else:
            names_normal.append(name.strip())


    if names_size == 0:
        return "Hello, my friend."
    if names_size == 1:
        if names[0] == None:
            return "Hello, my friend."
        else:
            return f"Hello {names[0]}."
    if names[0].isupper():
        return f"HELLO, {name}!"
    if names[0] != None and names[1] != None:
        return f"Hello, {names[0]} and {names[1]}."
    return f"Hello {names[0]}."
