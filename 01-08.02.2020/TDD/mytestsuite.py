import unittest
from test_basics import my_first_test

def suite():
    suite = unittest.TestSuite()
    suite.addTest(my_first_test("test_01"))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
