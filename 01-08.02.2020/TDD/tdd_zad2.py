import unittest
from greeter import *


class GreeterTestCase(unittest.TestCase):

    def test_check_if_functions_greets(self):
        self.assertEqual(greet("Patrycja"), "Hello Patrycja.")

    def test_greet_when_no_name_given(self):
        self.assertEqual(greet(None), "Hello, my friend.")

    def test_greet_with_shouting(self):
        self.assertEqual(greet("ANIA"), "HELLO, ANIA!")

    def test_greet_two_persons(self):
        self.assertEqual(greet("Patryk", "Ania"), "Hello, Patryk and Ania.")

    def test_greeting_with_multiple_names(self):
        self.assertEqual(greet("Patryk", "Ania", "Janusz", "Grażyna"), "Hello, Patryk, Ania, Janusz, and Grażyna")

    def test_greeting_with_mixed_case_names(self):
        self.assertEqual(greet("Patryk", "ANIA", "Janusz", "Grażyna", "SEBA"),
                         "Hello, Patryk, Janusz, and Grażyna. AND HELLO ANIA AND SEBA!")
        self.assertEqual(greet("Patryk", "Ania", "JANUSZ", "Grażyna"),
                         "Hello, Patryk, Ania, and Grażyna. AND HELLO JANUSZ!")

    def test_greet_when_names_given_as_list_in_string(self):
        self.assertEqual(greet("Patryk", "ANIA", "Janusz, Grażyna", "SEBA"),
                         "Hello, Patryk, Janusz, and Grażyna. AND HELLO ANIA AND SEBA!")
        self.assertEqual(greet("Patryk, Ania", "JANUSZ", "Grażyna"),
                         "Hello, Patryk, Ania, and Grażyna. AND HELLO JANUSZ!")


if __name__ == '__main__':
    unittest.main()
