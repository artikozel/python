import unittest


def setUpModule():
    print("module setup")

def tearDownModule():
    print("module teardown")


class my_first_test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("class set up")

    @classmethod
    def tearDownClass(cls):
        print("class teardown")

    def setUp(self):
        print("setting up for tests")

    def tearDown(self):
        print("tear down for test")

    def test_01(self):
        print("porównuje 2 i 3")
        self.assertEqual(2, 3)

    def test_02(self):
        print("porównuje 2 i 2")
        self.assertEqual(2, 2)


