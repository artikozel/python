class Car:
    acceleration = 10

    def __init__(self, registration_number):
        self.registration_number = registration_number
        self.in_motion = False
        self.speed = 0

    def drive(self):
        self.in_motion = True

    def accelerate(self):
        self.speed += self.acceleration

    def stop(self):
        self.in_motion = False
        self.speed = 0


def printCarData(car):
    print("")
    print("Nr. rejestr", car.registration_number)
    print("Czy w ruchu", car.in_motion)
    print("Predkosc", car.speed)
    print("Przyspieszenie", car.acceleration)
    print("")