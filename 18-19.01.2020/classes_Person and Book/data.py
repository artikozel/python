books = [
    ["Angole", "Ewa Winnicka"],
    ["Astrid Lindgren. Opowieść o życiu i twórczości", "Margareta Strömstedt"],
    ["Awantury na tle powszechnego ciążenia", "Tomasz Lem"],
    ["Bajka o Rašce i inne reportaże sportowe", "Ota Pavel"],
    ["Chłopczyce z Kabulu. Za kulisami buntu obyczajowego w Afganistanie", "Jenny Nordberg"],
    ["Czyje jest nasze życie? Olga Drenda", "Bartłomiej Dobroczyński"],
    ["Detroit. Sekcja zwłok Ameryki", "Charlie LeDuff"],
    ["Głód", "Martín Caparrós"],
    ["Hajstry. Krajobraz bocznych dróg", "Adam Robiński"],
    ["Jak rozmawiać o książkach", "których się nie czytało", "Pierre Bayard"],
    ["J jak jastrząb", "Helen Macdonald"],
    ["Łódź 370", "Annah Björk", "Mattias Beijmo"],
    ["Matka młodej matki", "Justyna Dąbrowska"],
    ["Mężczyźni objaśniają mi świat", "Rebecca Solnit"],
    ["Nie ma się czego bać", "Justyna Dąbrowska"],
    ["Non/fiction. Nieregularnik reporterski"],
    ["Obwód głowy", "Włodzimierz Nowak"],
    ["Ostatnie dziecko lasu", "Richard Louv"],
    ["Polska odwraca oczy", "Justyna Kopińska"],
    ["Powrócę jako piorun", "Maciej Jarkowiec"],
    ["Simona. Opowieść o niezwyczajnym życiu Simony Kossak", "Anna Kamińska"],
    ["Szlaki. Opowieści o wędrówkach", "Robert Macfarlane"],
    ["Wykluczeni", "Artur Domosławski"]
]
