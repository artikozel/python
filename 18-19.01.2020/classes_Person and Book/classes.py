class Person:
    def __init__(self, name, lastname, age=0):
        self.name = name
        self.lastname = lastname
        self.age = age
        self.favourite_books = []


    def can_i_buy_alcohol(self):
        return self.age >= 18


    def set_age(self, age):
        self.age = age


    def book_add(self, name):
        self.favourite_books.append(name)


    def book_remove(self, name):
        self.favourite_books.remove(name)


    def person_data(self):
        print(self.name, self.lastname, self.age, str(self.can_i_buy_alcohol()), self.favourite_books)

class Book:
    def __init__(self, name, author, isbn):
        self.name = name
        self.author = author
        self.isbn = isbn
