import random

from klasy import Osoba
from klasy import Ksiazka


list_of_books = [
["13 pięter", "Filip Springer"],

["Angole", "Ewa Winnicka"],

["Astrid Lindgren. Opowieść o życiu i twórczości", "Margareta Strömstedt"],

["Awantury na tle powszechnego ciążenia", "Tomasz Lem"],

["Bajka o Rašce i inne reportaże sportowe", "Ota Pavel"],

["Chłopczyce z Kabulu. Za kulisami buntu obyczajowego w Afganistanie", "Jenny Nordberg"],

["Czyje jest nasze życie?", "Olga Drenda, Bartłomiej Dobroczyński"],

["Detroit. Sekcja zwłok Ameryki", "Charlie LeDuff"],

["Głód", "Martín Caparrós"],

["Hajstry. Krajobraz bocznych dróg", "Adam Robiński"],

["Jak rozmawiać o książkach, których się nie czytało", "Pierre Bayard"],

["J jak jastrząb", "Helen Macdonald"],

["Łódź 370", "Annah Björk, Mattias Beijmo"],

["Matka młodej matki", "Justyna Dąbrowska"],

["Mężczyźni objaśniają mi świat", "Rebecca Solnit"],

["Nie ma się czego bać", "Justyna Dąbrowska"],

["Non/fiction", "Nieregularnik reporterski"],

["Obwód głowy", "Włodzimierz Nowak"],

["Ostatnie dziecko lasu", "Richard Louv"],

["Polska odwraca oczy", "Justyna Kopińska"],

["Powrócę jako piorun", "Maciej Jarkowiec"],

["Simona. Opowieść o niezwyczajnym życiu Simony Kossak", "Anna Kamińska"],

["Szlaki. Opowieści o wędrówkach", "Robert Macfarlane"],

["Wykluczeni", "Artur Domosławski"]
]

def isbn_generator():
    isbn = ""
    while len(isbn)<=12:
        isbn = isbn + str(random.randint(0,10))
    return isbn/

# print(isbn_generator())
#
# print(len(list_of_books))


def book_list_generator():
    book_list = []
    while len(book_list)<10:
        data = list_of_books[random.randint(0, len(list_of_books)-1)]
        book_list.append(Ksiazka(data[0], data[1]))
    return book_list

random_book_list = book_list_generator()

# print(random_book_list)

def book_adder(person):
    for book in random_book_list:
        if book in person.favourite_books:
            continue
        else:
            person.addbook(book)


# plaza = Ksiazka("Plaża", "Alex Garland")
# plaza.print_data()

tymon = Osoba("Tymon", "Szafran")
# tymon.print_data()
tymon.favourite_booklist()
tymon.print_data()
tymon.addbook(Ksiazka("Powrócę jako piorun", "Maciej Jarkowiec"))
tymon.addbook(Ksiazka("Powrócę jako piorun", "Maciej Jarkowiec"))
# book_adder(tymon)
tymon.favourite_booklist()
tymon.print_data()
tymon.removebook(Ksiazka("Powrócę jako piorun", "Maciej Jarkowiec"))
tymon.print_data()


# tymon.addbook(random_book_list[9])
# tymon.print_data()


# orwell = Ksiazka("1984", "George Orwell")
# tymon.addbook(orwell)
# tymon.addbook(Ksiazka("The Repossession Mambo", "Eric Garcia"))
print("=============")
# book_adder(tymon)tymon.print_data()


# tymon.print_data()
# print(list(tymon.favourite_books))




# for i in random_book_list:
#     ksiazka = Ksiazka(random_book_list[0], random_book_list[1], isbn_generator())
#     print(ksiazka)

# print(len(book_list_generator()))


#
# for elem in book_list:
#     print(elem[0:2])