from Vehicles import Car
from Vehicles import printCarData

if __name__ == '__main__':
    volvo = Car("VOLVO")
    audi = Car("AUDI")
    printCarData(volvo)
    printCarData(audi)

    print("uruchom Audi")
    audi.drive()
    print("przyspiesz Audi")
    audi.accelerate()

    printCarData(volvo)
    printCarData(audi)

    print("uruchom volvo")
    volvo.drive()
    print("przyspiesz volvo x10")
    for x in range(0, 10):
        volvo.accelerate()
    print("zatrzymaj audi")
    audi.stop()

    printCarData(volvo)
    printCarData(audi)
