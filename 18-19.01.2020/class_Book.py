class Book:
    books_total = 0

    def __init__(self, title, author):
        self.title = title
        self.author = author
        Book.books_total += 1
        self.id = Book.books_total

    def print_data(self):
        print(self.title, self.author, self.id)

repo = Book("The Repossession Mambo", "Eric Garcia")
orwell = Book("1984", "George Orwell")
plaza = Book("Plaża", "Alex Garland")

print(Book.books_total)
print(repo.id)
print(orwell.id)
print(plaza.id)
repo.print_data()
orwell.print_data()
plaza.print_data()
print("===============")
print(repo.books_total)
print(repo.books_total)
print(Book.books_total)
Book.books_total = 100
print(repo.books_total)
print(Book.books_total)
