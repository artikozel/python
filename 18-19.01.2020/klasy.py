import random

# Stworzyc klase Osoba
#
# - ktora w konstruktorze przyjmuje 2 wartosci Imie, Nazwisko
#
# - Domyslna wartosc wieku ustawic na 0 dla wszystkich osob
#
# - utowrztc metode ktora ustawia wiek
#
# - stworzyc metode o nazwie czyMogeKupicAlkohol zwraca True lub False w zaleznosci od wieku jaki posiada
#
# - posiada 2 motody dodaj/usun ulubione ksiazki

class Osoba:
    def __init__(self, name, lastname, age=0):
        self.name = name
        self.lastname = lastname
        self.age = age
        self.favourite_books = []

    def can_buy_alcohol(self):
        if self.age >= 18:
            return True
        else:
            return False


    def favourite_booklist(self):
        for elem in self.favourite_books:
            print(elem.name, elem.author, elem.ISBN)
            # elem.print_data()

    def addbook(self, book):
        self.favourite_books.append(book)

    def removebook(self, book):
        if book in self.favourite_books:
            self.favourite_books.remove(book)

    def change_age(self, age):
        self.age = age

    def print_data(self):
        print(self.name, self.lastname, self.age, self.can_buy_alcohol())
        print("Ulubione książki: ")
        for book in self.favourite_books:
            book.print_data()



# Stworzyc klase Ksiazka
#
# - ktora w konstruktorze przyjmuje wartosci NazwaKsiazki, Autor, ISBN

class Ksiazka:
    def __init__(self, book_name, author):
        self.name = book_name
        self.author = author
        self.ISBN = self.isbn()


    def print_data(self):
        print(self.name, self.author, str(self.ISBN))

    def isbn(self):
        isbn = ""
        while len(isbn) <= 12:
            isbn += str(random.randint(0, 10))
        return isbn



