# 1. Losowe ceny
#
# Stwórz funkcję add_product, która będzie przyjmować 2 argumenty: nazwę oraz cenę produktu.
# Funkcja do globalnej listy produktów będzie dodawała nowy produkt (reprezentowany przez 2-elementową krotkę (nazwa, cena)).
# Cena produktu powinna być argumentem domyślnym, tzn. możemy jej nie podawać
# - w takim przypadku powinna ona przyjąć wartość losową będącą parzystą liczbą z przedziału od 50 do 150 zł włącznie.
#
# Wykorzystaj tę funkcję, aby dodać kilka produktów. Do części wywołań funkcji add_product
# przekaż sztywno zdefiniowaną cenę w argumencie, a do reszty nie.
#
# Na sam koniec działania programu wypisz na ekran informacje o najdroższym oraz o najtańszym z produktów.

from random import *
product_list = []

def add_product(name, price=None):
    if price == None:
        #price = randrange(50, 151, 2)
        price = randint(25, 75) * 2

    product_list.append((name, price))

def get_price(obj):
    return obj[-1]


if __name__ == '__main__':
    add_product('mleko')
    add_product('bigos', 100)
    add_product('pasta do zębów')
    add_product('pasta do butów', 10)
    add_product('bułka', 1.5)

    print(product_list)
    product_list.sort(key=get_price)
    print(f'Najtańszy produkt to {product_list[0]}')
    print(f'Najdroższy produkt to {product_list[-1]}')



