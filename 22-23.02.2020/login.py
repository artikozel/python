# 1. Login
#
# Stwórz bazę danych loginów i haseł. Baza powinna być w formie słownika, gdzie login użytkownika jest kluczem,
# a skrót hasła wartością. Do wygenerowania skrótu hasła skorzystaj z funkcji sha1 z modułu hashlib, przykład:
#
# >>> import hashlib
# >>> hashlib.sha1('tajne hasło'.encode()).hexdigest()
# 'c77efceb47fe04deae36fee2115d6a74109bc930'
#
# Następnie napisz funkcję login, która będzie przyjmowała 2 argumenty nazwane: username oraz password.
# Funkcja sprawdzi czy podany użytkownik znajduje się w bazie - jeżeli nie to wypisze komunikat,
# że takiego użytkownika nie ma. Jeżeli taki użytkownik jest w bazie to sprawdzi czy zostało podane prawidłowe hasło -
# jeżeli nie to wypisze komunikat o błędnym haśle i natychmiastowo wyjdzie z programu (użyj funkcji exit z modułu sys).
# Jeżeli użytkownik podał dobry login i hasło to wyświetl komunikat powitania.
#
# Wykorzystaj funkcję login, przekazując do niej dane, które uprzednio użytkownik wprowadzi z klawiatury.
# Do podania hasła nie korzystaj z funkcji input lecz z funkcji getpass z modułu getpass
# (aczkolwiek może ona nie działać prawidłowo w PyCharmie - wtedy uruchom program z konsoli).


import hashlib
import sys

my_database = {'adrian': '58e71360c3359fdde64fef0b005892eb85443391',
               'kasia': '90d143a1e9a0eb30678d93ad22507ad35eb6d0da',
               'marta': '206e829f136ced612e6cebe7890bb1771407eb7c'}

def login(*, username, password):
    password = hashlib.sha1('tajne hasło'.encode()).hexdigest()
    if username not in my_database:
        print("Username does not exist")
        sys.exit()
     username in my_database:
        if my_database[username] == password:
            print("Login successful")


print('adrian' in my_database)