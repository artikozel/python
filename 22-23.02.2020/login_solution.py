"""
1. Login
Stwórz bazę danych loginów i haseł. Baza powinna być w formie słownika, gdzie login użytkownika jest kluczem, a skrót hasła wartością. Do wygenerowania skrótu hasła skorzystaj z funkcji sha1 z modułu hashlib, przykład:

import hashlib
hashlib.sha1('tajne hasło'.encode()).hexdigest()
'c77efceb47fe04deae36fee2115d6a74109bc930'
Następnie napisz funkcję login, która będzie przyjmowała 2 argumenty nazwane: username oraz password. Funkcja sprawdzi czy podany użytkownik znajduje się w bazie - jeżeli nie to wypisze komunikat, że takiego użytkownika nie ma. Jeżeli taki użytkownik jest w bazie to sprawdzi czy zostało podane prawidłowe hasło - jeżeli nie to wypisze komunikat o błędnym haśle i natychmiastowo wyjdzie z programu (użyj funkcji exit z modułu sys). Jeżeli użytkownik podał dobry login i hasło to wyświetl komunikat powitania.

Wykorzystaj funkcję login, przekazując do niej dane, które uprzednio użytkownik wprowadzi z klawiatury. Do podania hasła nie korzystaj z funkcji input lecz z funkcji getpass z modułu getpass (aczkolwiek może ona nie działać prawidłowo w PyCharmie - wtedy uruchom program z konsoli).
"""
import hashlib
import sys
from getpass import getpass

database = {
    'kjura': '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a',
}


def login(*, username, password):
    if username not in database:
        print('Nie ma takiego użytkownika')
        sys.exit()

    password = hashlib.sha1(password.encode()).hexdigest()

    if database[username] != password:
        print('Złe hasło!')
        sys.exit()

    print(f'Witaj {username}, zalogowałeś się!')


if __name__ == '__main__':
    username = input('username:')
    password = getpass('password:')
    login(username=username, password=password)

