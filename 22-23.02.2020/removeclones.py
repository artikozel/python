"""
# 1. Usuwanie klonów
Stwórz funkcję, która przyjmie w argumencie listę imion. Funkcja powinna wypisać na ekran unikalne imiona z listy, posortowane malejąco.
"""

names = ['Adam', 'Jacek', 'Placek', 'Gacek', 'Adam']

def print_sorted_without_clones(names):
    unique_names = list(set(names))
    unique_names.sort(reverse=True)
    print('\n'.join(unique_names))
    # for name in unique_names:
    #     print(name)
    #
def print_sorted_without_clones1(names):
    unique_names = list(set(names))
    reversed_names = reversed(unique_names)
    print(list(reversed_names))
    # for name in unique_names:
    #     print(name)

def print_sorted_without_clones2(names):
    unique_names = set(names)
    unique_names = sorted(unique_names, reverse=True)
    print('\n'.join(unique_names))

def print_sorted_without_clones3(names):
    """
    Ta funkcja sortuje elementy według długości
    """
    unique_names = list(set(names))
    unique_names.sort(reverse=True, key=len)
    print('\n'.join(unique_names))

def key_last_letter(obj):

    return obj[-1]

def print_sorted_without_clones4(names):
    """
    Ta funkcja sortuje elementy po wartości ostatniej literki
    """
    unique_names = list(set(names))
    unique_names.sort(reverse=True, key=key_last_letter)
    print('\n'.join(unique_names))

def key_length(obj):
    return len(obj)

def print_sorted_without_clones5(names):
    """
    Ta funkcja sortuje elementy według długości
    """
    unique_names = list(set(names))
    unique_names.sort(reverse=True, key=key_length)
    print('\n'.join(unique_names))

if __name__ == '__main__':
    print_sorted_without_clones1(names)

