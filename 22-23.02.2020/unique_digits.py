#
# 2. Unikalne czy nieunikalne, oto jest pytanie
#
# Stwórz funkcję has_only_unique_digits, która przyjmie w argumencie liczbę
# i zwróci True jeżeli żadna z cyfr w liczbie się nie powtarza, a False w przeciwnym razie.

def has_only_unique_digits(number):
    number = str(number)
    return len(set(number)) == len(number)

print(has_only_unique_digits(100))
print(has_only_unique_digits(103))



#
# def has_only_unique_digits(n):
#     unique=list(str(n))
#     if len(unique) == len(set(unique)):
#         return True
#     else:
#         return False
#
# if __name__=='__main__':
#     print(has_only_unique_digits(1234567))

