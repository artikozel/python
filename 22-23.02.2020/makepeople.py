'''1
. Masowe tworzenie ludzi

Stwórz funkcję make_people, która będzie przyjmować 2 argumenty:

    *names - imiona nowych ludzi
    **kwargs - atrybuty nowych ludzi - można przyjąć, że mogą to być tylko atrybuty: age (wiek) oraz fav_color (ulubiony kolor)

Funkcja powinna stworzyć listę ludzi, z których każdy będzie reprezentowany przez słownik z kluczami name, age, fav_color.
Jeżeli użytkownik nie przekaże wieku oraz ulubionego koloru to nadaj im wartości domyślne.

Dodatkowo użytkownik w argumentach age oraz fav_color powinien móc przekazać listę -
w takim wypadku wartość danego atrybutu dla tworzonego człowieka powinna być losową wartością z tej listy.'''
import random

# def make_people(*names, age=10, fav_colour='yellow'):
#     people_list = []
#     for name in names:
#         people_list.append({'name': name, 'age': age, 'favourite colour': fav_colour})
#     print(people_list)
#
# make_people('Tomek','Rafał','Alina', age=25, fav_colour='blue')


def make_people(*names, **kwargs):
    people = []
    for name in names:
        age = kwargs.get('age', 1)
        if isinstance(age, list):
            age = random.choice(age)

        fav_color = kwargs.get('fav_color', 'black')
        if isinstance(fav_color, list):
            # fav_colors = fav_color
            fav_color = random.choice(fav_color)
            # fav_colors.remove(fav_color)

        person = {
            'name': name,
            'age': age,
            'fav_color': fav_color
        }
        people.append(person)

    return people


people = make_people(
    'Dyzio', 'Hyzio', 'Zyzio', age=5,
    fav_color=['red', 'green', 'blue']
)

for person in people:
    print(person)

