'''4. Czy element jest w zbiorze?

Dlaczego dla dużych list staramy się nie sprawdzać czy zawierają one dany element, a zamiast tego,
dla tego zastosowania powinniśmy użyć zbiorów?

Wykorzystaj funkcję timeit z modułu timeit i sprawdź ile czasu zajmie na Twoim komputerze milion operacji sprawdzenia,
czy element znajduje się dużej liście / zbiorze (o wielkości np. 100_000).
Sprawdź czy czas działania zmienia się w przypadku, gdy zmienia się element, którego szukamy.
'''

from timeit import timeit


my_list = list(range(100000))
my_set = set(my_list)

def find_in_list(x):
    return x in my_list

def find_in_set(x):
    return x in my_set

print(timeit(
    stmt='find_in_set(689)',
    # setup='kod_przygotowujacy_ktorego_czas_nie_jest_mierzony',
    globals=globals(),
    number=1000000,
))

print(timeit(
    stmt='find_in_list(689)',
    # setup='kod_przygotowujacy_ktorego_czas_nie_jest_mierzony',
    globals=globals(),
    number=1000000,
))

print(timeit(
    stmt='find_in_set(68999)',
    # setup='kod_przygotowujacy_ktorego_czas_nie_jest_mierzony',
    globals=globals(),
    number=1000000,
))

print(timeit(
    stmt='find_in_list(68999)',
    # setup='kod_przygotowujacy_ktorego_czas_nie_jest_mierzony',
    globals=globals(),
    number=10000,
))


# """
# # 4. Czy element jest w zbiorze?
#
# Dlaczego dla dużych list staramy się nie sprawdzać czy zawierają one dany element, a zamiast tego, dla tego zastosowania powinniśmy użyć zbiorów?
#
# Wykorzystaj funkcję timeit z modułu timeit i sprawdź ile czasu zajmie na Twoim komputerze milion operacji sprawdzenia, czy element znajduje się dużej liście / zbiorze (o wielkości np. 100_000). Sprawdź czy czas działania zmienia się w przypadku, gdy zmienia się element, którego szukamy.
# """
#
# from timeit import timeit
#
# print(timeit(
#     stmt='2.5 in x',
#     setup='x = set(range(100_000))',
#     number=1_000_000,
# ))
#
# print(timeit(
#     stmt='2.5 in x',
#     setup='x = list(range(100_000))',
#     number=1000,
# ))



