'''
1. Minął rok, a dane stare...

Jako, że na ulicy Sezamkowej jest dzisiaj sylwester, to należy zaktualizować dane w plikach .csv, aby były gotowe na jutro.

Stwórz plik .csv z następującymi danymi:

imie;nazwisko;wiek;film
Jacek;Wróbel;42;Piraci z Karaibów
Łukasz;Podniebny-Chodziarz;89;Gwiezdne Wojny
Bilbo;Bagosz;301;Hobbit
Gerard;Zriwi;115;Wiedźmak

Przy pomocy Pythona zwiększ wiek każdej postaci o 1.
'''

with open('/home/kozel/PycharmProjects/python/22-23.02.2020/dane.csv', 'r') as file:
    for i in file:
        print(i)

'''
rows = []

with open('file.csv', 'r', encoding='utf-8') as file:
    for line in file:
        rows.append(line.split(';'))

for row in rows[1:]:
    row[2] = str(int(row[2]) + 1)

with open('file.csv', 'w', encoding='utf-8') as file:
    for row in rows:
        file.write(';'.join(row))'''


