"""
# 1. Déjà vu...

Jako, że w dolinie Muminków jest dzisiaj sylwester, to należy utworzyć aktualne pliki z danymi tekstowymi, aby były gotowe na jutro.

Stwórz plik .txt z następującymi danymi:

Jacek Wróbel 42
Łukasz Podniebny-Chodziarz 89
Bilbo Bagosz 301
Gerard Zriwi 115
Przy pomocy Pythona zwiększ wiek każdej postaci o 1, a dane zapisz w nowym pliku.

Jak to zrobić tym razem?

W Pythonie zmień standardowy strumień wejścia tak, aby funkcja input czytała z powyższego pliku.
Następnie zmień standardowy strumień wyjścia, aby funkcja print zapisywała zaktualizowane dane do nowego pliku tekstowego.
"""
import sys
sys.stdin = open('dane.txt', encoding='utf-8')
sys.stdout = open('out.txt', 'w', encoding='utf-8')
# 3 powyższe linijki, albo wykonanie programu jak poniżej:
# python test.py <in.txt >out.txt

rows = []

while True:
    try:
        rows.append(input().split())
    except EOFError:
        break

for row in rows:
    row[2] = str(int(row[2]) + 1)
    print(' '.join(row))
