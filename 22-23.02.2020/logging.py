'''
3. Tworzenie logów

Stwórz funkcję log zapisującą dane do pliku z logami log.txt.
Funkcja powinna przyjmować jeden argument - tekst zapisywany do pliku,
ale oprócz niego powinna też zapisywać bieżącą datę i czas (skorzystaj z datetime.now() z modułu datetime):

 >> from datetime import datetime
 >> str(datetime.now())
'2020-02-22 01:23:15.375580'

Następnie stwórz kilka funkcji Pythonowych i na początku zapisuj do pliku, że została ona wykonana.
Zrób to za pomocą wykonania funkcji log z odpowiednim argumentem.

Następnie kilkukrotnie wykonaj stworzone funkcje i sprawdź czy zawartość pliku log.txt -
powinna zawierać informacje o wykonanych funkcjach.

Uwaga! Python posiada dedykowany moduł logging pomocny podczas tworzenia logów -
w przyszłości, zamiast logować dane w taki sposób jak w zadaniu, to możemy z niego skorzystać.'''

from datetime import datetime

def log(message):
    with open('log.txt', 'a') as file:
        file.write(f'{datetime.now()} {message}\n')

def func1():
    log('Uruchomiono func1')

def func2():
    log('Uruchomiono func2')

def func3():
    log('Uruchomiono func3')


if __name__ == '__main__':
    func1()
    func2()
    func3()
    func3()
    func2()

