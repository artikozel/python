"""
# 3. Jam jest bardziej unikalna

Stwórz funkcję `more_unique`, która będzie przyjmować 2 argumenty będące liczbami. Funkcja powinna zwrócić tą z dwóch liczb, która posiada więcej unikalnych cyfr. W przypadku, gdy obie mają ich tyle samo - powinna zostać zwrócona liczba, której suma cyfr jest mniejsza.
"""


def digits_sum(num):
    result = 0
    for digit in num:
        result += int(digit)
    return result


def more_unique(num1, num2):
    num1 = str(num1)
    num2 = str(num2)

    unique_digits1 = set(num1)
    unique_digits2 = set(num2)

    if len(unique_digits1) > len(unique_digits2):
        return int(num1)

    elif len(unique_digits1) < len(unique_digits2):
        return int(num2)

    else:
        digits_sum1 = digits_sum(num1)
        digits_sum2 = digits_sum(num2)

        if digits_sum1 < digits_sum2:
            return int(num1)
        elif digits_sum1 > digits_sum2:
            return int(num2)


def more_unique2(num1, num2):
    num1_str = str(num1)
    num2_str = str(num2)

    elements = [
        (len(set(num1_str)), -digits_sum(num1_str), num1),
        (len(set(num2_str)), -digits_sum(num2_str), num2),
    ]

    elements.sort()

    my_tuple = elements[-1]
    return my_tuple[2]


def my_key(obj):
    obj = str(obj)
    return [len(set(obj)), -digits_sum(obj)]

def more_unique3(num1, num2):
    return max([num1, num2], key=my_key)


if __name__ == '__main__':
    # print(more_unique(100, 102))
    # print(more_unique(110, 100))

    # print(more_unique2(100, 102))
    # print(more_unique2(110, 100))

    # print(more_unique3(100, 102))
    # print(more_unique3(110, 100))

    print(more_unique3(204, 105))










