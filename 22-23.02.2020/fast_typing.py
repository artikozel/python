'''2. Granko w szybkie pisanko

Twoim zadaniem jest stworzenie gry, która będzie mierzyła i wyświetlała na ekran:
najszybszy, najwolniejszy, średni i sumaryczny czas jaki użytkownik potrzebował na wpisanie pewnych słów na ekran.

Listę dostępnych w naszej grze słów weźmiemy ze słownika języka Polskiego dostępnego tutaj,
który jest kodowany w systemie ISO8859-2 (ta informacja będzie Ci potrzebna podczas otwierania pliku).

Z powyższego pliku wczytaj do listy tylko wyrazy o długości 7.
Podczas sprawdzania długości wyrazu nie bierz pod uwagę znaków nowej linii i zwróć uwagę,
ze w niektórych liniach po słowie występuje zbędny ukośnik, a po nim zbędne znaczki.

Gra będzie polegała na poprawnym wpisaniu 10 losowych słów z wczytanej z pliku bazy słów
(użyj funkcji choice z modułu random). Jeżeli użytkownik popełni literówkę (lub wpisze słowo błędnie)
to powinien wpisywać dane słowo aż do skutku. Do mierzenia czasu wykorzystaj funkcje perf_counter z modułu time:

 import time
 start = time.perf_counter()

...

 end = time.perf_counter()
 end - start
7.250810164026916

Na koniec wyświetl najszybszy czas wpisania wyrazu, najwolniejszy czas wpisania wyrazu
oraz sumę i średnią czasów dla wszystkich wyrazów. Każdą z wartości wypisz zaokrągloną do 3 miejsc po przecinku.'''

import time
import random

words = []
with open('PL.txt', 'r', encoding='ISO8859-2') as file:
    for line in file:
        word = line.split("/")[0]
        word = word.strip()

        if len(word) == 7 and word.isalpha():
            words.append(word)
times = []

print("Wpisuj słowa!")
for _ in range(3):
    start = time.perf_counter()
    word = random.choice(words)
    while True:
        user_word = input(f'{word}: ')
        if user_word == word:
            break
        print("Literówka, podaj ponownie!")
    end = time.perf_counter()
    times.append(end - start)

print(f'Najwolniej: {max(times):.3f}')
print(f'Najszybciej: {min(times):.3f}')
print(f'Sumarycznie: {sum(times):.3f}')
print(f'Średnio: {sum(times) / len(times):.3f}')



