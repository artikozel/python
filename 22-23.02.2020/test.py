def tag(name, *contents, _class=None, **attrs):
    if _class is not None:
        attrs['class'] = _class

    attrs_html = []
    for key, value in attrs.items():
        attrs_html.append(f' {key}="{value}"')
    attrs_html = ''.join(attrs_html)

    tags = []
    if contents:
        for content in contents:
            tags.append(f'<{name}{attrs_html}>{content}</{name}>')
    else:
        tags.append(f'<{name}{attrs_html} />')

    return ''.join(tags)

if __name__ == '__main__':
    print(tag('p', 'hello world', _class='red', id=5))